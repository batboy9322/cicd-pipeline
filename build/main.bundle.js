/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/calculator/toCelsius.js":
/*!*************************************!*\
  !*** ./src/calculator/toCelsius.js ***!
  \*************************************/
/***/ ((module) => {

eval("/**\r\n * Farenheit to Celsius \r\n */\nfunction toCelsius(firstNum) {\n  var a = (firstNum - 32) * 5 / 9;\n  return a;\n}\nmodule.exports = toCelsius;\n\n//# sourceURL=webpack://cicd-pipeline/./src/calculator/toCelsius.js?");

/***/ }),

/***/ "./src/calculator/toFarenheit.js":
/*!***************************************!*\
  !*** ./src/calculator/toFarenheit.js ***!
  \***************************************/
/***/ ((module) => {

eval("/**\r\n * Celsius to Farenheit\r\n */\nfunction toFarenheit(firstNum) {\n  return firstNum * (9 / 5) + 32;\n}\nmodule.exports = toFarenheit;\n\n//# sourceURL=webpack://cicd-pipeline/./src/calculator/toFarenheit.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/***/ ((__unused_webpack_module, __unused_webpack_exports, __webpack_require__) => {

eval("// import files\nvar toCelsius = __webpack_require__(/*! ./calculator/toCelsius */ \"./src/calculator/toCelsius.js\");\nvar toFarenheit = __webpack_require__(/*! ./calculator/toFarenheit */ \"./src/calculator/toFarenheit.js\");\nwindow.onload = function () {\n  document.getElementById(\"calculate\").addEventListener(\"click\", calculate);\n};\nfunction calculate() {\n  var firstNum = parseInt(document.getElementById(\"firstnumber\").value);\n  var operator = document.getElementById(\"operator\").value;\n  var resultElm = document.getElementById(\"result\");\n  var result;\n  try {\n    switch (operator) {\n      case \"toC\":\n        result = toCelsius(firstNum);\n        break;\n      case \"toF\":\n        result = toFarenheit(firstNum);\n        break;\n      default:\n        alert(\"Error!\");\n        break;\n    }\n    resultElm.textContent = \"Result: \" + result;\n    resultElm.style.color = \"black\";\n  } catch (error) {\n    resultElm.textContent = \"Error: \" + error.message;\n    resultElm.style.color = \"red\";\n  }\n}\n\n//# sourceURL=webpack://cicd-pipeline/./src/index.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/index.js");
/******/ 	
/******/ })()
;