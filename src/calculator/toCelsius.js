/**
 * Farenheit to Celsius 
 */
 function toCelsius(firstNum) {
  var a = ((firstNum - 32) * 5/9);
  return a;
}
module.exports = toCelsius