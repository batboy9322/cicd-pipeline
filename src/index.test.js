// import files
var toCelsius = require('./calculator/toCelsius')
var toFarenheit = require('./calculator/toFarenheit')


// Test Case 1 (Farenheit to Celsius)
test("Should convert Farenheit to Celsius", () => {
  expect(toCelsius(212)).toBe(100);
});

test("Should convert Farenheit to Celsius", () => {
  expect(toCelsius(101)).toBe(38.333333333333336);
});

// Test Case 3 (Celsius to Farenheit)
test("Should convert Celsius to Farenheit", () => {
  expect(toFarenheit(100)).toBe(212);
});