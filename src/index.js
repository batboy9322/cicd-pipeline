// import files
var toCelsius = require("./calculator/toCelsius");
var toFarenheit = require("./calculator/toFarenheit");

window.onload = function () {
  document.getElementById("calculate").addEventListener("click", calculate);
};

function calculate() {
  var firstNum = parseInt(document.getElementById("firstnumber").value);
  var operator = document.getElementById("operator").value;
  var resultElm = document.getElementById("result");
  var result;

  try {
    switch (operator) {
      case "toC":
        result = toCelsius(firstNum);
        break;
      case "toF":
        result = toFarenheit(firstNum);
        break;
      default:
        alert("Error!");
        break;
    }
    resultElm.textContent = "Result: " + result;
    resultElm.style.color = "black";
  } catch (error) {
    resultElm.textContent = "Error: " + error.message;
    resultElm.style.color = "red";
  }
}
